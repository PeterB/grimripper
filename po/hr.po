# Croatian translation for GrimRipper
# Copyright (C) 2016
# This file is distributed under the same license as the GrimRipper package.
# Petar Kulić <cooleech@gmail.com>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: 2.8\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-14 13:22+0100\n"
"PO-Revision-Date: 2016-01-08 10:11+0100\n"
"Last-Translator: Petar Kulić <cooleech@gmail.com>\n"
"Language-Team: Hrvatski <LL@li.org>\n"
"Language: Croatian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural= n!=1;\n"

#: src/main.c:131 src/interface.c:230
msgid "Rip"
msgstr "Ripaj"

#: src/main.c:141
msgid "Track"
msgstr "Zapis"

#: src/main.c:149
msgid "Artist"
msgstr "Izvođač"

#: src/main.c:157 src/main.c:165
msgid "Title"
msgstr "Naslov"

#: src/main.c:173
msgid "Time"
msgstr "Vrijeme"

#: src/main.c:199
msgid ""
"'cdparanoia' was not found in your path. GrimRipper requires cdparanoia to rip "
"CDs."
msgstr ""
"'cdparanoia' nije pronađen na vašem sustavu. GrimRipperu treba cdparanoia kako "
"biripao CD-e."

#: src/main.c:244 src/main.c:269 src/main.c:869
msgid "<b>Checking disc...</b>"
msgstr ""

#: src/main.c:538
msgid "<b>Getting disc info from the internet...</b>"
msgstr "<b>Skidam info o disku s interneta...</b>"

#: src/main.c:599
msgid "<b>Reading disc...</b>"
msgstr ""

#: src/callbacks.c:61 src/callbacks.c:319 src/callbacks.c:351
#: src/callbacks.c:362 src/callbacks.c:373 src/callbacks.c:385
#: src/interface.c:606 src/interface.c:685 src/interface.c:784
#: src/interface.c:913
#, c-format
msgid "%dKbps"
msgstr "%dKbps"

#: src/callbacks.c:750
msgid "No CD is inserted. Please insert a CD into the CD-ROM drive."
msgstr "Nema CD-a. Molim, ubacite CD u optički uređaj."

#: src/callbacks.c:771 src/callbacks.c:799 src/callbacks.c:826
#: src/callbacks.c:853 src/callbacks.c:880 src/callbacks.c:908
#: src/callbacks.c:935 src/callbacks.c:962
#, c-format
msgid ""
"%s was not found in your path. GrimRipper requires it to create %s files. All %s "
"functionality is disabled."
msgstr ""
"%s nije pronađen na sustavu. GrimRipperu je potreban kako bi stvorio %s "
"datoteke. Sva%s funkcionalnost je onemogućena."

#: src/callbacks.c:1060
msgid "Select all for ripping"
msgstr "Označi sve za ripanje"

#: src/callbacks.c:1066
msgid "Deselect all for ripping"
msgstr "Odznači sve za ripanje"

#: src/callbacks.c:1072
msgid "Capitalize Artists & Titles"
msgstr ""

#: src/callbacks.c:1102
msgid "Swap Artist <=> Title"
msgstr ""

#: src/interface.c:85
msgid "CDDB Lookup"
msgstr "CDDB pretraga"

#: src/interface.c:91 src/interface.c:372
msgid "Preferences"
msgstr "Postavke"

#: src/interface.c:102
msgid "About"
msgstr ""

#: src/interface.c:135
msgid "Disc:"
msgstr "Disk"

#: src/interface.c:139
msgid "Album Artist:"
msgstr "Izvođač albuma:"

#: src/interface.c:144
msgid "Album Title:"
msgstr "Naslov albuma:"

#: src/interface.c:149
msgid "Single Artist"
msgstr "Jedan izvođač"

#: src/interface.c:157
msgid "First track number:"
msgstr ""

#: src/interface.c:170
msgid "Track number width in filename:"
msgstr ""

#: src/interface.c:197
msgid "Genre / Year:"
msgstr "Vrsta / Godina:"

#: src/interface.c:389 src/interface.c:395
msgid "Destination folder"
msgstr "Odredišni folder"

#: src/interface.c:399
msgid "Create M3U playlist"
msgstr "Napravi M3U listu"

#: src/interface.c:407
msgid "CD-ROM device: "
msgstr "Optički uređaj: "

#: src/interface.c:416
msgid ""
"Default: /dev/cdrom\n"
"Other example: /dev/hdc\n"
"Other example: /dev/sr0"
msgstr ""
"Uobičajeno: /dev/cdrom\n"
"Drugi primjer: /dev/hdc\n"
"Drugi primjer: /dev/sr0"

#: src/interface.c:420
msgid "Eject disc when finished"
msgstr "Izbaci disk po završetku"

#: src/interface.c:428
msgid "General"
msgstr "Općenito"

#: src/interface.c:448
msgid ""
"%A - Artist\n"
"%L - Album\n"
"%N - Track number (2-digit)\n"
"%Y - Year (4-digit or \"0\")\n"
"%T - Song title"
msgstr ""
"%A - Izvođač\n"
"%L - Album\n"
"%N - Redni broj (dvoznamenkasti)\n"
"%Y - Godina (četveroznamenkasta ili \"0\")\n"
"%T - Naslov pjesme"

#: src/interface.c:453
#, c-format
msgid "%G - Genre"
msgstr "%G - Vrsta"

#: src/interface.c:469
msgid "Album directory: "
msgstr "Direktorij albuma: "

#: src/interface.c:474 src/prefs.c:776
msgid "Playlist file: "
msgstr "Datoteka liste: "

#: src/interface.c:479 src/prefs.c:788 src/prefs.c:798
msgid "Music file: "
msgstr "Glazbena datoteka: "

#: src/interface.c:490
msgid ""
"This is relative to the destination folder (from the General tab).\n"
"Can be blank.\n"
"Default: %A - %L\n"
"Other example: %A/%L"
msgstr ""
"Ovo je putanja do odredišnog foldera (prema karitci Općenito).\n"
"Može biti prazno.\n"
"Uobičajeno: %A - %L\n"
"Drugi primjer: %A/%L"

#: src/interface.c:500
msgid ""
"This will be stored in the album directory.\n"
"Can be blank.\n"
"Default: %A - %L"
msgstr ""
"Ovo će biti pohranjeno u direktoriju albuma.\n"
"Može biti prazno.\n"
"Uobičajeno: %A - %L"

#: src/interface.c:509
msgid ""
"This will be stored in the album directory.\n"
"Cannot be blank.\n"
"Default: %A - %T\n"
"Other example: %N - %T"
msgstr ""
"Ovo će biti pohranjeno u direktoriju albuma.\n"
"Ne može biti prazno.\n"
"Uobičajeno: %A - %T\n"
"Drugi primjer: %N - %T"

#: src/interface.c:514
msgid "Filename formats"
msgstr "Format imena datoteka"

#: src/interface.c:519
msgid "Allow changing first track number"
msgstr ""

#: src/interface.c:524
msgid "Filenames"
msgstr "Imena datoteka"

#: src/interface.c:558
msgid "WAV (uncompressed)"
msgstr "WAV (bez kompresije)"

#: src/interface.c:576
msgid "Variable bit rate (VBR)"
msgstr "Varijabilna gustoća bitova (VBR)"

#: src/interface.c:583
msgid "Better quality for the same size."
msgstr "Bolja kvaliteta za istu veličinu."

#: src/interface.c:589 src/interface.c:668 src/interface.c:769
#: src/interface.c:851 src/interface.c:896
msgid "Bitrate"
msgstr "Gustoća bitova"

#: src/interface.c:603 src/interface.c:682
msgid ""
"Higher bitrate is better quality but also bigger file. Most people use "
"192Kbps."
msgstr ""
"Veća gustoća bitova daje bolju kvalitetu ali i veću datoteku. Većina ljudi "
"koristi 192Kbps."

#: src/interface.c:612
msgid "MP3 (lossy compression)"
msgstr "MP3 (kompresija uz gubitke)"

#: src/interface.c:632
msgid "Quality"
msgstr "Kvaliteta"

#: src/interface.c:642
msgid "Higher quality means bigger file. Default is 6."
msgstr "Veća kvaliteta znači veću datoteku. Uobičajeno je 6."

#: src/interface.c:644
msgid "OGG Vorbis (lossy compression)"
msgstr "OGG Vorbis (kompresija uz gubitke)"

#: src/interface.c:691
#, fuzzy
msgid "AAC (lossy compression)"
msgstr "MP3 (kompresija uz gubitke)"

#: src/interface.c:711 src/interface.c:817 src/interface.c:949
msgid "Compression level"
msgstr "Razina kompresije"

#: src/interface.c:721 src/interface.c:961
msgid "This does not affect the quality. Higher number means smaller file."
msgstr "Ovo ne utječe na kvalitetu. Veći broj znači manju datoteku."

#: src/interface.c:723
msgid "FLAC (lossless compression)"
msgstr "FLAC (kompresija bez gubitaka)"

#: src/interface.c:741
msgid "More formats"
msgstr "Više formata"

#: src/interface.c:782
msgid ""
"Higher bitrate is better quality but also bigger file. Most people use "
"160Kbps."
msgstr ""
"Veća gustoća bitova daje bolju kvalitetu ali i veću datoteku. Većina ljudi "
"koristi 160Kbps."

#: src/interface.c:790
msgid "OPUS (lossy compression)"
msgstr "OPUS (kompresija uz gubitke)"

#: src/interface.c:830
msgid ""
"This does not affect the quality. Higher number means smaller file. Default "
"is 1 (recommended)."
msgstr ""
"Ovo ne utječe na kvalitetu. Veći broj znači manju datoteku. Uobičajeno je 1 "
"(preporučeno)."

#: src/interface.c:836
msgid "Hybrid compression"
msgstr "Hibridna kompresija"

#: src/interface.c:845
msgid ""
"The format is lossy but a correction file is created for restoring the "
"lossless original."
msgstr ""
"Format ima gubitaka ali datoteka oporavka je stvorena da bi se vratila "
"originalna kvaliteta."

#: src/interface.c:911
msgid "Higher bitrate is better quality but also bigger file."
msgstr "Veća gustoća bitova daje bolju kvalitetu ali i veću datoteku."

#: src/interface.c:919
msgid "Musepack (lossy compression)"
msgstr "Musepack (kompresija uz gubitke)"

#: src/interface.c:963
msgid "Monkey's Audio (lossless compression)"
msgstr "Monkey's Audio (kompresija bez gubitaka)"

#: src/interface.c:981
msgid "Encode"
msgstr "Enkodiraj"

#: src/interface.c:1015
msgid "Get disc info from the internet automatically"
msgstr "Automatski skini info o disku s interneta"

#: src/interface.c:1023 src/interface.c:1067
msgid "Server: "
msgstr "Poslužitelj: "

#: src/interface.c:1032
msgid "The CDDB server to get disc info from (default is gnudb.gnudb.org)"
msgstr "CDDB poslužitelj za info o disku (uobičajeno je gnudb.gnudb.org)"

#: src/interface.c:1038 src/interface.c:1080
msgid "Port: "
msgstr "Port: "

#: src/interface.c:1047
msgid "The CDDB server port (default is 8880)"
msgstr "Port CDDB poslužitelja (uobičajeno je 8880)"

#: src/interface.c:1054
msgid "Use an HTTP proxy to connect to the internet"
msgstr "Koristi HTTP proxy za spajanje na internet"

#: src/interface.c:1102
msgid "Artist/Title separator: "
msgstr ""

#: src/interface.c:1113
msgid "Log to /var/log/grimripper.log"
msgstr "Zapis u /var/log/grimripper.log"

#: src/interface.c:1118
msgid "Faster ripping (no error correction)"
msgstr "Brže ripanje (bez ispravki grešaka)"

#: src/interface.c:1127
msgid "Advanced"
msgstr "Napredno"

#: src/interface.c:1183 src/interface.c:1219
msgid "Ripping"
msgstr "Ripanje"

#: src/interface.c:1213
msgid "Total progress"
msgstr "Ukupan napredak"

#: src/interface.c:1225
msgid "Encoding"
msgstr "Enkodiram"

#: src/interface.c:1865
#, c-format
msgid "%d file created successfully"
msgid_plural "%d files created successfully"
msgstr[0] "%d datoteka je uspješno zapisana"
msgstr[1] "%d datoteke su uspješno zapisane"

#: src/interface.c:1874
#, c-format
msgid "There was an error creating %d file"
msgid_plural "There was an error creating %d files"
msgstr[0] "Nastala je greška prilikom stvaranja %d datoteke"
msgstr[1] "Nastala je greška prilikom stvaranja %d datoteka"

#: src/prefs.c:775 src/prefs.c:787
#, c-format
msgid "Invalid characters in the '%s' field"
msgstr "Nedozvoljen znak u '%s' polju"

#: src/prefs.c:797
#, c-format
msgid "'%s' cannot be empty"
msgstr "'%s' ne može biti prazno"

#: src/prefs.c:812
msgid "Invalid proxy port number"
msgstr "Nevažeći broj proxy porta"

#: src/prefs.c:825
msgid "Invalid cddb server port number"
msgstr "Nevažeći broj porta cddb poslužitelja"

#: src/support.c:47
msgid "Overwrite?"
msgstr "Prepisati?"

#: src/support.c:60
#, c-format
msgid "The file '%s' already exists. Do you want to overwrite it?\n"
msgstr "Datoteka '%s' već postoji. Želite li pisati preko nje?\n"

#: src/support.c:66
msgid "Remember the answer for _all the files made from this CD"
msgstr "Zapamti odgovor za _sve datoteke s ovog diska"

#: src/threads.c:187
msgid ""
"No ripping/encoding method selected. Please enable one from the "
"'Preferences' menu."
msgstr ""
"Nema odabrane metode za ripanje/enkodiranje. Molim odaberite jednu u meniju "
"'Postavke'."

#: src/threads.c:219
msgid ""
"No tracks were selected for ripping/encoding. Please select at least one "
"track."
msgstr ""
"Nema odabranih zapisa za ripanje/enkodiranje. Molim odaberite bar jedan "
"zapis."

#: src/threads.c:1299 src/threads.c:1301 src/threads.c:1305
msgid "Waiting..."
msgstr "Čekam..."

#, fuzzy
#~ msgid ""
#~ "An application to save tracks from an Audio CD \n"
#~ "as WAV, MP3, OGG, FLAC, Wavpack, Opus, Musepack, Monkey's Audio, and/or "
#~ "AAC files."
#~ msgstr ""
#~ "Aplikacija za spremanje zapisa s Audio CD-a \n"
#~ "u WAV, MP3, OGG, FLAC, Opus, Wavpack, Musepack, Monkey's Audio, i/ili AAC "
#~ "datoteke."

#~ msgid "Proprietary encoders"
#~ msgstr "Vlasnički enkoderi"

#~ msgid "Higher quality means bigger file. Default is 60."
#~ msgstr "Veća kvaliteta znači veću datoteku. Uobičajeno je 60."

#~ msgid "AAC (lossy compression, Nero encoder)"
#~ msgstr "AAC (kompresija uz gubitke, Nero enkoder)"

#~ msgid "Genre"
#~ msgstr "Vrsta"

#~ msgid "Single Genre"
#~ msgstr "Jedna vrsta"
