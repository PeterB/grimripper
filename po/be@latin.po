# Biełaruski pierakład GrimRipper.
# Copyright (C) 2008 Ihar Hrachyshka
# This file is distributed under the same license as the grimripper package.
# Ihar Hrachyshka <ihar.hrachyshka@gmail.com>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: grimripper\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-14 13:22+0100\n"
"PO-Revision-Date: 2008-01-06 01:33+0200\n"
"Last-Translator: Ihar Hrachyshka <ihar.hrachyshka@gmail.com>\n"
"Language-Team: i18n@mova.org <i18n@mova.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Belarusian latin\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: src/main.c:131 src/interface.c:230
msgid "Rip"
msgstr "Ździary"

#: src/main.c:141
msgid "Track"
msgstr "Numar pieśni"

#: src/main.c:149
msgid "Artist"
msgstr "Vykanaŭca"

#: src/main.c:157 src/main.c:165
msgid "Title"
msgstr "Nazva"

#: src/main.c:173
msgid "Time"
msgstr "Čas"

#: src/main.c:199
msgid ""
"'cdparanoia' was not found in your path. GrimRipper requires cdparanoia to rip "
"CDs."
msgstr ""
"\"cdparanoia\" nia znojdzienaja ŭ ściežcy. GrimRipper vymahaje \"cdparanoia\", "
"kab ździrać huk z dyskaŭ."

#: src/main.c:244 src/main.c:269 src/main.c:869
msgid "<b>Checking disc...</b>"
msgstr ""

#: src/main.c:538
msgid "<b>Getting disc info from the internet...</b>"
msgstr "<b>Atrymańnie źviestak ab dysku ź sieciva...</b>"

#: src/main.c:599
msgid "<b>Reading disc...</b>"
msgstr ""

#: src/callbacks.c:61 src/callbacks.c:319 src/callbacks.c:351
#: src/callbacks.c:362 src/callbacks.c:373 src/callbacks.c:385
#: src/interface.c:606 src/interface.c:685 src/interface.c:784
#: src/interface.c:913
#, fuzzy, c-format
msgid "%dKbps"
msgstr "32Kbps"

#: src/callbacks.c:750
msgid "No CD is inserted. Please insert a CD into the CD-ROM drive."
msgstr "Dysk nia ŭstaŭleny. Kali łaska, ustaŭ dysk u pryładu CD-ROM."

#: src/callbacks.c:771 src/callbacks.c:799 src/callbacks.c:826
#: src/callbacks.c:853 src/callbacks.c:880 src/callbacks.c:908
#: src/callbacks.c:935 src/callbacks.c:962
#, c-format
msgid ""
"%s was not found in your path. GrimRipper requires it to create %s files. All %s "
"functionality is disabled."
msgstr ""
"%s nia znojdzienaja ŭ ściežcy. GrimRipper vymahaje jaje, kab stvarać fajły %s. "
"Usie funkcyi %s vyklučanyja."

#: src/callbacks.c:1060
msgid "Select all for ripping"
msgstr "Zaznač usio, kab ździerci"

#: src/callbacks.c:1066
msgid "Deselect all for ripping"
msgstr "Anuluj zaznačeńnie ździranaha"

#: src/callbacks.c:1072
msgid "Capitalize Artists & Titles"
msgstr ""

#: src/callbacks.c:1102
msgid "Swap Artist <=> Title"
msgstr ""

#: src/interface.c:85
msgid "CDDB Lookup"
msgstr "Pošuk CDDB"

#: src/interface.c:91 src/interface.c:372
msgid "Preferences"
msgstr "Nałady"

#: src/interface.c:102
msgid "About"
msgstr ""

#: src/interface.c:135
msgid "Disc:"
msgstr "Dysk:"

#: src/interface.c:139
#, fuzzy
msgid "Album Artist:"
msgstr "Vykanaŭca albomu:"

#: src/interface.c:144
#, fuzzy
msgid "Album Title:"
msgstr "Nazva albomu:"

#: src/interface.c:149
#, fuzzy
msgid "Single Artist"
msgstr "Adzin vykanaŭca"

#: src/interface.c:157
msgid "First track number:"
msgstr ""

#: src/interface.c:170
msgid "Track number width in filename:"
msgstr ""

#: src/interface.c:197
#, fuzzy
msgid "Genre / Year:"
msgstr "Ahulnaje"

#: src/interface.c:389 src/interface.c:395
msgid "Destination folder"
msgstr "Metavy kataloh"

#: src/interface.c:399
msgid "Create M3U playlist"
msgstr "Stvary śpis M3U"

#: src/interface.c:407
msgid "CD-ROM device: "
msgstr "Pryłada CD-ROM: "

#: src/interface.c:416
msgid ""
"Default: /dev/cdrom\n"
"Other example: /dev/hdc\n"
"Other example: /dev/sr0"
msgstr ""
"Zmoŭčana: /dev/cdrom\n"
"Inšy prykład: /dev/hdc\n"
"Inšy prykład: /dev/sr0"

#: src/interface.c:420
msgid "Eject disc when finished"
msgstr "Vysuń dysk pa zakančeńni"

#: src/interface.c:428
msgid "General"
msgstr "Ahulnaje"

#: src/interface.c:448
#, fuzzy
msgid ""
"%A - Artist\n"
"%L - Album\n"
"%N - Track number (2-digit)\n"
"%Y - Year (4-digit or \"0\")\n"
"%T - Song title"
msgstr ""
"%A - Vykanaŭca\n"
"%L - Albom\n"
"%N - Numar pieśni (2-ličbavy)\n"
"%T - Nazva pieśni"

#: src/interface.c:453
#, fuzzy, c-format
msgid "%G - Genre"
msgstr "Ahulnaje"

#: src/interface.c:469
msgid "Album directory: "
msgstr "Kataloh albomu: "

#: src/interface.c:474 src/prefs.c:776
msgid "Playlist file: "
msgstr "Fajł śpisu: "

#: src/interface.c:479 src/prefs.c:788 src/prefs.c:798
msgid "Music file: "
msgstr "Muzyčny fajł: "

#: src/interface.c:490
msgid ""
"This is relative to the destination folder (from the General tab).\n"
"Can be blank.\n"
"Default: %A - %L\n"
"Other example: %A/%L"
msgstr ""
"Adnosna metavaha katalohu (z kartki Ahulnyja).\n"
"Moža być pustym.\n"
"Zmoŭčany: %A - %L\n"
"Inšy prykład: %A/%L"

#: src/interface.c:500
msgid ""
"This will be stored in the album directory.\n"
"Can be blank.\n"
"Default: %A - %L"
msgstr ""
"Budzie zachoŭvacca ŭ katalohu albomu.\n"
"Moža być pustym.\n"
"Zmoŭčany: %A - %L"

#: src/interface.c:509
msgid ""
"This will be stored in the album directory.\n"
"Cannot be blank.\n"
"Default: %A - %T\n"
"Other example: %N - %T"
msgstr ""
"Budzie zachoŭvacca ŭ katalohu albomu.\n"
"Moža być pustym.\n"
"Zmoŭčany: %A - %T\n"
"Inšy prykład: %N - %T"

#: src/interface.c:514
msgid "Filename formats"
msgstr "Farmaty nazvaŭ fajłaŭ"

#: src/interface.c:519
msgid "Allow changing first track number"
msgstr ""

#: src/interface.c:524
msgid "Filenames"
msgstr "Nazvy fajłaŭ"

#: src/interface.c:558
msgid "WAV (uncompressed)"
msgstr "WAV (skampresavany)"

#: src/interface.c:576
msgid "Variable bit rate (VBR)"
msgstr "Źmiennaja častata bitaŭ (VBR)"

#: src/interface.c:583
msgid "Better quality for the same size."
msgstr "Lepšaja jakaść dla takoha samaha pamieru."

#: src/interface.c:589 src/interface.c:668 src/interface.c:769
#: src/interface.c:851 src/interface.c:896
msgid "Bitrate"
msgstr "Častata bitaŭ"

#: src/interface.c:603 src/interface.c:682
msgid ""
"Higher bitrate is better quality but also bigger file. Most people use "
"192Kbps."
msgstr ""
"Vyšejšaja častata bitaŭ aznačaje lepšuju jakaść, ale i bolšy pamier. Bolšaść "
"ludziej vykarystoŭvaje 192Kbps."

#: src/interface.c:612
msgid "MP3 (lossy compression)"
msgstr "MP3 (stratnaja kampresija)"

#: src/interface.c:632
msgid "Quality"
msgstr "Jakaść"

#: src/interface.c:642
msgid "Higher quality means bigger file. Default is 6."
msgstr "Vyšejšaja jakaść aznačaje bolšyja fajły. Zmoŭčanaja vartaść 6."

#: src/interface.c:644
msgid "OGG Vorbis (lossy compression)"
msgstr "OGG Vorbis (stratnaja kampresija)"

#: src/interface.c:691
#, fuzzy
msgid "AAC (lossy compression)"
msgstr "MP3 (stratnaja kampresija)"

#: src/interface.c:711 src/interface.c:817 src/interface.c:949
msgid "Compression level"
msgstr "Uzrovień kampresii"

#: src/interface.c:721 src/interface.c:961
msgid "This does not affect the quality. Higher number means smaller file."
msgstr "Heta nie ŭpłyvaje na jakaść. Bolšaja vartaść aznačaje mienšy fajł."

#: src/interface.c:723
msgid "FLAC (lossless compression)"
msgstr "FLAC (biasstratnaja kampresija)"

#: src/interface.c:741
msgid "More formats"
msgstr "Bolej farmataŭ"

#: src/interface.c:782
#, fuzzy
msgid ""
"Higher bitrate is better quality but also bigger file. Most people use "
"160Kbps."
msgstr ""
"Vyšejšaja častata bitaŭ aznačaje lepšuju jakaść, ale i bolšy pamier. Bolšaść "
"ludziej vykarystoŭvaje 192Kbps."

#: src/interface.c:790
#, fuzzy
msgid "OPUS (lossy compression)"
msgstr "MP3 (stratnaja kampresija)"

#: src/interface.c:830
msgid ""
"This does not affect the quality. Higher number means smaller file. Default "
"is 1 (recommended)."
msgstr ""
"Heta nie ŭpłyvaje na jakaść. Bolšaja vartaść aznačaje mienšy fajł. "
"Zmoŭčanaja vartaść 1 (rekamendavanaja)."

#: src/interface.c:836
msgid "Hybrid compression"
msgstr "Hibrydnaja kampresija"

#: src/interface.c:845
msgid ""
"The format is lossy but a correction file is created for restoring the "
"lossless original."
msgstr ""
"Farmat niastratny, ale dla ŭznaŭleńnia byłoj jakaści stvarajecca asobny fajł "
"vypraŭleńniaŭ."

#: src/interface.c:911
#, fuzzy
msgid "Higher bitrate is better quality but also bigger file."
msgstr ""
"Vyšejšaja častata bitaŭ aznačaje lepšuju jakaść, ale i bolšy pamier. Bolšaść "
"ludziej vykarystoŭvaje 192Kbps."

#: src/interface.c:919
#, fuzzy
msgid "Musepack (lossy compression)"
msgstr "MP3 (stratnaja kampresija)"

#: src/interface.c:963
#, fuzzy
msgid "Monkey's Audio (lossless compression)"
msgstr "FLAC (biasstratnaja kampresija)"

#: src/interface.c:981
msgid "Encode"
msgstr "Zakaduj"

#: src/interface.c:1015
msgid "Get disc info from the internet automatically"
msgstr "Atrymlivaj źviestki ab dysku ź sievica aŭtamatyčna"

#: src/interface.c:1023 src/interface.c:1067
msgid "Server: "
msgstr "Server: "

#: src/interface.c:1032
msgid "The CDDB server to get disc info from (default is gnudb.gnudb.org)"
msgstr ""

#: src/interface.c:1038 src/interface.c:1080
msgid "Port: "
msgstr "Port: "

#: src/interface.c:1047
msgid "The CDDB server port (default is 8880)"
msgstr ""

#: src/interface.c:1054
#, fuzzy
msgid "Use an HTTP proxy to connect to the internet"
msgstr "Užyj proxy HTTP, kab spałučycca ź siecivam"

#: src/interface.c:1102
msgid "Artist/Title separator: "
msgstr ""

#: src/interface.c:1113
msgid "Log to /var/log/grimripper.log"
msgstr ""

#: src/interface.c:1118
msgid "Faster ripping (no error correction)"
msgstr ""

#: src/interface.c:1127
msgid "Advanced"
msgstr "Prasunutyja"

#: src/interface.c:1183 src/interface.c:1219
msgid "Ripping"
msgstr "Ździrańnie"

#: src/interface.c:1213
msgid "Total progress"
msgstr "Ahulny prahres"

#: src/interface.c:1225
msgid "Encoding"
msgstr "Kadavańnie"

#: src/interface.c:1865
#, c-format
msgid "%d file created successfully"
msgid_plural "%d files created successfully"
msgstr[0] "%d fajł paśpiachova stvorany"
msgstr[1] "%d fajły paśpiachova stvoranyja"
msgstr[2] "%d fajłaŭ paśpiachova stvoranyja"

#: src/interface.c:1874
#, c-format
msgid "There was an error creating %d file"
msgid_plural "There was an error creating %d files"
msgstr[0] "Pamyłka pry stvareńni %d fajłu"
msgstr[1] "Pamyłka pry stvareńni %d fajłaŭ"
msgstr[2] "Pamyłka pry stvareńni %d fajłaŭ"

#: src/prefs.c:775 src/prefs.c:787
#, c-format
msgid "Invalid characters in the '%s' field"
msgstr "Niapravilnyja znaki ŭ poli \"%s\""

#: src/prefs.c:797
#, c-format
msgid "'%s' cannot be empty"
msgstr "\"%s\" nia moža być pustym"

#: src/prefs.c:812
msgid "Invalid proxy port number"
msgstr "Niapravilny numar portu proxy"

#: src/prefs.c:825
#, fuzzy
msgid "Invalid cddb server port number"
msgstr "Niapravilny numar portu proxy"

#: src/support.c:47
msgid "Overwrite?"
msgstr "Nadpisać?"

#: src/support.c:60
#, c-format
msgid "The file '%s' already exists. Do you want to overwrite it?\n"
msgstr "Fajł \"%s\" užo isnuje. Chočaš nadpisać?\n"

#: src/support.c:66
msgid "Remember the answer for _all the files made from this CD"
msgstr "Zapomni adkaz dla _ŭsich fajłaŭ, zroblenych z hetaha dysku"

#: src/threads.c:187
msgid ""
"No ripping/encoding method selected. Please enable one from the "
"'Preferences' menu."
msgstr ""
"Metad ździrańnia/kadavańnia nia vybrany. Kali łaska, uklučy adzin ź ich u "
"menu \"Nałady\"."

#: src/threads.c:219
msgid ""
"No tracks were selected for ripping/encoding. Please select at least one "
"track."
msgstr ""
"Dla ździrańnia/kadavańnia nie zaznačana pieśniaŭ. Kali łaska, zaznač chacia "
"b adnu ź pieśniaŭ."

#: src/threads.c:1299 src/threads.c:1301 src/threads.c:1305
msgid "Waiting..."
msgstr "Čakańnie..."

#, fuzzy
#~ msgid "Higher quality means bigger file. Default is 60."
#~ msgstr "Vyšejšaja jakaść aznačaje bolšyja fajły. Zmoŭčanaja vartaść 6."

#, fuzzy
#~ msgid "AAC (lossy compression, Nero encoder)"
#~ msgstr "MP3 (stratnaja kampresija)"

#, fuzzy
#~ msgid "Genre"
#~ msgstr "Ahulnaje"

#, fuzzy
#~ msgid "Single Genre"
#~ msgstr "Adzin vykanaŭca"

#, fuzzy
#~ msgid ""
#~ "%s was not found in your path. GrimRipper requires it to create %s files.All "
#~ "%s functionality is disabled."
#~ msgstr ""
#~ "%s nia znojdzienaja ŭ ściežcy. GrimRipper vymahaje jaje, kab stvarać fajły "
#~ "%s. Usie funkcyi %s vyklučanyja."
